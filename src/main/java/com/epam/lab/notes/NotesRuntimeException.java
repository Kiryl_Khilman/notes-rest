package com.epam.lab.notes;

public class NotesRuntimeException extends RuntimeException {

    public NotesRuntimeException(String message) {
        super(message);
    }

    public NotesRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotesRuntimeException(Throwable cause) {
        super(cause);
    }
}
