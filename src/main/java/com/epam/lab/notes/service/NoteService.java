package com.epam.lab.notes.service;

import com.epam.lab.notes.domain.entity.Note;

import java.util.List;

public interface NoteService {

    void saveNote(Note note);

    void removeNote(Long noteId);

    List<Note> findNotesByCategory(Long categoryId);

}
