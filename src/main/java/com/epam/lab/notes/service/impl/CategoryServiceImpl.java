package com.epam.lab.notes.service.impl;

import com.epam.lab.notes.dao.CategoryDao;
import com.epam.lab.notes.domain.entity.Category;
import com.epam.lab.notes.service.CategoryService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CategoryServiceImpl implements CategoryService {

    private final CategoryDao categoryDao;

    public CategoryServiceImpl(CategoryDao categoryDao) {
        this.categoryDao = categoryDao;
    }

    @Override
    public void createCategory(Category category) {
        categoryDao.save(category);
    }

    @Override
    public void removeCategoryById(Long id) {
        categoryDao.deleteById(id);
    }

    @Override
    public Category getCategoryById(Long id) {
        Optional<Category> optionalCategory = categoryDao.findById(id);
        return optionalCategory.get();
    }
}
