package com.epam.lab.notes.service.impl;

import com.epam.lab.notes.dao.UserDao;
import com.epam.lab.notes.domain.entity.User;
import com.epam.lab.notes.service.UserService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private final UserDao userDao;
    private final PasswordEncoder passwordEncoder;

    public UserServiceImpl(UserDao userDao, PasswordEncoder passwordEncoder) {
        this.userDao = userDao;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void registerUser(User user) {
        String password = user.getPassword();
        String encodedPassword = passwordEncoder.encode(password);
        user.setPassword(encodedPassword);
        userDao.save(user);
    }

    @Override
    public User getUserById(Long id) {
        Optional<User> optionalUser = userDao.findById(id);
        return optionalUser.get();
    }
}
