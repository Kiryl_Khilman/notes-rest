package com.epam.lab.notes.service;

import com.epam.lab.notes.NotesRuntimeException;

public class DataNotFoundException extends NotesRuntimeException {
    public DataNotFoundException(String message) {
        super(message);
    }

    public DataNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public DataNotFoundException(Throwable cause) {
        super(cause);
    }
}
