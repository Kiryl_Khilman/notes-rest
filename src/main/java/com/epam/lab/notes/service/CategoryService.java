package com.epam.lab.notes.service;

import com.epam.lab.notes.domain.entity.Category;

public interface CategoryService {

    void createCategory(Category category);

    void removeCategoryById(Long id);

    Category getCategoryById(Long id);
}
