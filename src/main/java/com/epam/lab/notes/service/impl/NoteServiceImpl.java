package com.epam.lab.notes.service.impl;

import com.epam.lab.notes.dao.NoteDao;
import com.epam.lab.notes.domain.entity.Note;
import com.epam.lab.notes.service.NoteService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NoteServiceImpl implements NoteService {

    private final NoteDao noteDao;

    public NoteServiceImpl(NoteDao noteDao) {
        this.noteDao = noteDao;
    }

    @Override
    public void saveNote(Note note) {
        noteDao.save(note);
    }

    @Override
    public void removeNote(Long id) {
        noteDao.deleteById(id);
    }

    @Override
    public List<Note> findNotesByCategory(Long categoryId) {
       return noteDao.findAllByCategoryId(categoryId);
    }
}
