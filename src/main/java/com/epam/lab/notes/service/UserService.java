package com.epam.lab.notes.service;

import com.epam.lab.notes.domain.entity.User;

import java.util.Optional;

public interface UserService {

    void registerUser(User user);

    User getUserById(Long id);
}
