package com.epam.lab.notes.controller;

import com.epam.lab.notes.domain.dto.CategoryDto;
import com.epam.lab.notes.domain.entity.Category;
import com.epam.lab.notes.domain.entity.User;
import com.epam.lab.notes.domain.entity.UserPrincipal;
import com.epam.lab.notes.service.CategoryService;
import com.epam.lab.notes.service.UserService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class CategoryController {

    private final CategoryService categoryService;
    private final UserService userService;

    public CategoryController(CategoryService categoryService, UserService userService) {
        this.categoryService = categoryService;
        this.userService = userService;
    }

    @PostMapping("/category")
    public Category addCategory(Category category, @AuthenticationPrincipal UserPrincipal principal) {
        Long currentUserId = principal.getId();
        User user = userService.getUserById(currentUserId);
        category.setUser(user);
        categoryService.createCategory(category);
        return category;
    }

    @DeleteMapping("/{categoryId}")
    @PreAuthorize("hasPermission(#categoryId, 'com.epam.lab.notes.entity.Category', 'OWNER')")
    public void removeCategory(@PathVariable Long categoryId) {
        categoryService.removeCategoryById(categoryId);
    }

    @GetMapping("/{categoryId}")
    @PreAuthorize("hasPermission(#categoryId, 'com.epam.lab.notes.entity.Category', 'OWNER')")
    public CategoryDto getCategory(@PathVariable Long categoryId) {
        Category category = categoryService.getCategoryById(categoryId);
        return new CategoryDto(category);
    }

    @GetMapping("/category")
    public List<Category> getAllUserCategories(@AuthenticationPrincipal UserPrincipal userPrincipal) {
        Long currentUserId = userPrincipal.getId();
        User user = userService.getUserById(currentUserId);
        return user.getCategories();
    }
}
