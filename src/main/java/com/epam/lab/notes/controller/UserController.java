package com.epam.lab.notes.controller;

import com.epam.lab.notes.domain.entity.User;
import com.epam.lab.notes.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/register")
    public String register(User user) {
        userService.registerUser(user);
        return "forward:/login";
    }
}
