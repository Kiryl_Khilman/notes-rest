package com.epam.lab.notes.controller;

import com.epam.lab.notes.domain.entity.Category;
import com.epam.lab.notes.domain.entity.Note;
import com.epam.lab.notes.service.CategoryService;
import com.epam.lab.notes.service.NoteService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class NoteController {

    private final NoteService noteService;
    private final CategoryService categoryService;

    public NoteController(NoteService noteService, CategoryService categoryService) {
        this.noteService = noteService;
        this.categoryService = categoryService;
    }

    @PostMapping("/{categoryId}")
    public Note createNote(@PathVariable Long categoryId, Note note) {
        Category category = categoryService.getCategoryById(categoryId);
        note.setCategory(category);
        noteService.saveNote(note);
        return note;
    }

    @PutMapping("/{categoryId}/{noteId}")
    @PreAuthorize("#note.category.user.id == principal.id")
    public void updateNote (@PathVariable Long categoryId, Note note) {
        Category category = categoryService.getCategoryById(categoryId);
        note.setCategory(category);
        noteService.saveNote(note);
    }

    @DeleteMapping("/{categoryId}/{noteId}")
    @PreAuthorize("hasPermission(#noteId, 'com.epam.lab.notes.entity.Note', 'OWNER')")
    public void removeNote(@PathVariable Long noteId) {
        noteService.removeNote(noteId);
    }
}
