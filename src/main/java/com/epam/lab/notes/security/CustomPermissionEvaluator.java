package com.epam.lab.notes.security;

import com.epam.lab.notes.dao.CategoryDao;
import com.epam.lab.notes.dao.NoteDao;
import com.epam.lab.notes.domain.entity.Category;
import com.epam.lab.notes.domain.entity.Note;
import com.epam.lab.notes.domain.entity.UserPrincipal;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Optional;

@Component
public class CustomPermissionEvaluator implements PermissionEvaluator {

    private final NoteDao noteDao;
    private final CategoryDao categoryDao;

    public CustomPermissionEvaluator(NoteDao noteDao, CategoryDao categoryDao) {
        this.noteDao = noteDao;
        this.categoryDao = categoryDao;
    }

    @Override
    public boolean hasPermission(Authentication authentication, Object targetDomainObject, Object permission) {
        Long currentUserId = getCurrentUserId(authentication);
        if (targetDomainObject instanceof Note) {
            Note note = (Note) targetDomainObject;
            Long noteOwnerId = note
                    .getCategory()
                    .getUser()
                    .getId();
            return currentUserId.equals(noteOwnerId);
        } else
            if (targetDomainObject instanceof Category) {
                Category category = (Category) targetDomainObject;
                Long categoryOwner = category
                        .getUser()
                        .getId();
                return currentUserId.equals(categoryOwner);

        }
        return false;
    }

    private Long getCurrentUserId(Authentication authentication) {
        UserPrincipal principal = (UserPrincipal) authentication.getPrincipal();
        return principal.getId();
    }

    @Override
    public boolean hasPermission(Authentication authentication, Serializable targetId, String targetType, Object permission) {
        String noteType = Note.class.getName();
        Long currentUserId = getCurrentUserId(authentication);
        if (noteType.equals(targetType)) {
            Optional<Note> optionalNote = noteDao.findById(((Long) targetId));
            if (optionalNote.isPresent()) {
                Note note = optionalNote.get();
                Long ownerId = note
                        .getCategory()
                        .getUser()
                        .getId();
                return currentUserId.equals(ownerId);
            }
        } else if (Category.class.getName().equals(targetType)) {

            Optional<Category> optionalCategory = categoryDao.findById((Long) targetId);
            if (optionalCategory.isPresent()) {
                Category category = optionalCategory.get();
                Long ownerId = category
                        .getUser()
                        .getId();
                return currentUserId.equals(ownerId);
            }
        }
        return false;
    }
}
