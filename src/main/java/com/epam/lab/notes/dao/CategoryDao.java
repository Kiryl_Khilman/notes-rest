package com.epam.lab.notes.dao;

import com.epam.lab.notes.domain.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryDao extends JpaRepository<Category, Long> {

}
