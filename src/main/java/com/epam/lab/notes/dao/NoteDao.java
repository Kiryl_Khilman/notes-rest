package com.epam.lab.notes.dao;

import com.epam.lab.notes.domain.entity.Note;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NoteDao extends JpaRepository<Note, Long> {

    List<Note> findAllByCategoryId(Long categoryId);
}
