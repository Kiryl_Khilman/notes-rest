package com.epam.lab.notes.domain.dto;

import com.epam.lab.notes.domain.entity.Note;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class NoteDto implements Serializable {

    private Long id;
    private String title;
    private Date date;

    public NoteDto(Long id, String title, Date date) {
        this.id = id;
        this.title = title;
        this.date = date;
    }

    public NoteDto(Note note) {
        this(note.getId(), note.getTitle(), note.getDate());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NoteDto noteDto = (NoteDto) o;
        return Objects.equals(id, noteDto.id) &&
                Objects.equals(title, noteDto.title) &&
                Objects.equals(date, noteDto.date);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, title, date);
    }

    @Override
    public String toString() {
        return "NoteDto{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", date=" + date +
                '}';
    }
}
