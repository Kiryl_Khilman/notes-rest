package com.epam.lab.notes.domain.dto;

import com.epam.lab.notes.domain.entity.Category;
import com.epam.lab.notes.domain.entity.Note;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class CategoryDto {

    private Long id;
    private String name;
    private List<NoteDto> notes;

    public CategoryDto(Long id, String name, List<Note> notes) {
        this.id = id;
        this.name = name;
        this.notes = notes.stream()
                .map(NoteDto::new)
                .collect(Collectors.toList());
    }

    public CategoryDto(Category category) {
        this(category.getId(), category.getName(), category.getNotes());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<NoteDto> getNotes() {
        return notes;
    }

    public void setNotes(List<NoteDto> notes) {
        this.notes = notes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CategoryDto that = (CategoryDto) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(notes, that.notes);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name, notes);
    }

    @Override
    public String toString() {
        return "CategoryDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", notes=" + notes +
                '}';
    }
}
